"""
Snake game.

1. We need a snake, it will initially occupy 1 box
2. We need a point, it will make the snake grow by 1 box
3. We a rule tha if the snake moves to the walls or over itself, it will die

We should import keyboard
"""
import numpy as np
import time
import random
import keyboard


class Game():
    """General game class.

    It incoporates a function that checks wether the game is finished or not
    """

    def __init__(self):
        self.score = 0
        self.endgame = False
        self.direction = np.array([0, 1])
#       Arbitrary variable to test
        self.i = 0

    def new_game(self):
        print("Starting new game")
        self.board = Board()
        while not self.check_endgame():
            self.move()
        print("Game finished with score:")
        print(self.score)

    def update_direction(self, letter):
        dir_trans = {'a': np.array([-1, 0]),
                     'w': np.array([0, 1]),
                     's': np.array([0, -1]),
                     'd': np.array([1, 0])}
        self.direction = dir_trans[letter]
        return

    def move(self):
        keyboard.add_hotkey('w', self.update_direction, args=['w'])
        keyboard.add_hotkey('a', self.update_direction, args=['a'])
        keyboard.add_hotkey('s', self.update_direction, args=['s'])
        keyboard.add_hotkey('d', self.update_direction, args=['d'])
        time.sleep(0.2)
        self.board.set_snake_position(self.direction)
        self.i += 1
        return

    def check_endgame(self):
        if self.board.check_crash():
            return True


class Board:
    """Class that incorporates the board in which the game is played.

    It includes a checker of wether an object has been hitted or not.
    """

    def __init__(self):
        self.size = [300, 300]
        self.snake = Snake()
        self.snake.generate_snake_position(self.size[0])
        self.item = self.generate_item()

    def set_snake_position(self, direction):
        self.snake.set_position(direction)
        return

    def generate_item(self):
        return np.array([random.randrange(0, self.size[0]),
                         random.randrange(0, self.size[0])])

    def check_crash(self):
        print("Checking snake pos")
        if np.any(self.snake.position >= 300):
            print("Gone above")
            return True
        elif np.any(self.snake.position <= 0):
            print("Gone below")
            return True
        # Non efficient way to check wether there has been a colision with body
        # for body_part in self.snake.position[1:]:
        #     if body_part == self.snake.position[1:]:
        #         return True


class Snake:
    def __init__(self):
        self.length = 1
        self.crash = False
        self.position = None

    def generate_snake_position(self, size):
        print("Initial snake position")
        self.position = np.array([random.randrange(0, size),
                                  random.randrange(0, size)])
        print(self.position)

    def set_position(self, displacement):
        print("Moving snake to new position")
        self.position += displacement
        print(self.position)

#    def check_crash(self):
#        for snake_boxes in


game = Game()
game.new_game()
